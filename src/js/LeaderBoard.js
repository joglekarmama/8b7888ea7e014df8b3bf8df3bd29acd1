import Phaser from 'phaser';
import Database from 'database-api';
import config from 'visual-config-exposer';

let database = new Database();

class LeaderBoard extends Phaser.Scene {
  constructor() {
    super('leaderBoard');
    this.GAME_WIDTH = 740;
    this.GAME_HEIGHT = 960;
    this.scores = [];
  }

  preload() {
    database
      .getLeaderBoard()
      .then((dataset) => {
        let sortedData = dataset.data.sort(
          (a, b) => parseInt(a.score) < parseInt(b.score)
        );
        this.scores = [...sortedData];
      })
      .catch((err) => {
        console.log(err);
      });
  }

  create() {
    const bg = this.add
      .image(0, 0, 'bg')
      .setOrigin(0, 0)
      .setScale(config.preGame.bgImgScaleX, config.preGame.bgImgScaleY);

    const title = this.add.text(
      this.GAME_WIDTH / 2.7,
      this.GAME_HEIGHT / 10,
      'LeaderBoard',
      {
        fontFamily: 'Helvetica',
        fontSize: '32px',
        fontStyle: 'bold',
        fill: '#000',
      }
    );

    let i,
      length = this.scores.length <= 10 ? this.scores.length : 10;
    for (i = 0; i < length; i++) {
      this.add.text(
        this.GAME_WIDTH / 4,
        this.GAME_HEIGHT / 7 + 40 * (i + 1),
        `${i + 1}         ${this.scores[i].display_name}        ${
          this.scores[i].score
        }`,
        {
          fontSize: '25px',
          fontFamily: 'Helvetica',
          fill: '#000',
        }
      );
    }

    let hoverImage = this.add.image(100, 100, 'startBall');
    hoverImage.setVisible(false);
    hoverImage.setScale(0.05);

    const backBtn = this.add.text(
      this.GAME_WIDTH / 2.8,
      this.GAME_HEIGHT / 1.5,
      'Back To Menu',
      {
        fontSize: '32px',
        fill: '#000',
      }
    );

    backBtn.setInteractive();

    backBtn.on('pointerover', () => {
      hoverImage.setVisible(true);
      hoverImage.x = backBtn.x - backBtn.width / 2.6;
      hoverImage.y = backBtn.y + 10;
    });
    backBtn.on('pointerout', () => {
      hoverImage.setVisible(false);
      // console.log('Play Out');
    });
    backBtn.on('pointerdown', () => {
      // console.log('redirect to play');
      this.scene.start('bootGame');
    });
  }
}

export default LeaderBoard;

//
//
//
//
//
//
// this.load.bitmapFont('arcade', 'assets/arcade.png', 'assets/arcade.xml');

// this.add
//   .bitmapText(100, 110, 'arcade', 'RANK  SCORE   NAME')
//   .setTint(0xffffff);

// for (let i = 1; i < 6; i++) {
//   if (scores[i - 1]) {
//     this.add
//       .bitmapText(
//         100,
//         160 + 50 * i,
//         'arcade',
//         ` ${i}      ${scores[i - 1].highScore}    ${scores[i - 1].name}`
//       )
//       .setTint(0xffffff);
//   } else {
//     this.add
//       .bitmapText(100, 160 + 50 * i, 'arcade', ` ${i}      0    ---`)
//       .setTint(0xffffff);
//   }
// }
